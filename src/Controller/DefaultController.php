<?php

namespace Drupal\filter_term\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default controller for the filter_term module.
 */
class DefaultController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs for Default controller.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(FormBuilderInterface $form_builder, Request $current_request, UserStorageInterface $user_storage, Connection $database) {
    $this->formBuilder = $form_builder;
    $this->currentRequest = $current_request;
    $this->userStorage = $user_storage;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('database')
    );
  }

  /**
   * Function to get nid from list of tid.
   */
  public function content() {
    $form = $this->formBuilder->getForm('\Drupal\filter_term\Form\VocabForm');

    $base_path = $this->currentRequest->getBaseUrl();
    $title = $this->currentRequest->query->get('title');
    $content_type = $this->currentRequest->query->get('content_type');
    $vocabulary = $this->currentRequest->query->get('vocabulary');
    $term_data = $this->currentRequest->query->get('term');
    $user_id = $this->currentRequest->query->get('users');
    if (!empty($user_id)) {
      $user = $this->userStorage->load($user_id);
      $user_name = $user->get('name')->value;
    }

    $header = [
      ['data' => $this->t('Title'), 'field' => 'title', 'sort' => 'asc'],
      ['data' => $this->t('Content type'), 'field' => 'type', 'sort' => 'asc'],
      ['data' => $this->t('Author'), 'field' => 'name', 'sort' => 'asc'],
      ['data' => $this->t('Status'), 'field' => 'status', 'sort' => 'asc'],
      ['data' => $this->t('Operations')],
    ];

    if (!empty($vocabulary) and  empty($term_data) and empty($content_type)) {
      $query = $this->database->select('taxonomy_index', 'ti');
      $query->join('node_field_data', 'nfd', 'nfd.nid = ti.nid');
      $query->join('users_field_data', 'u', 'n.uid=u.uid');
      $query->fields('u', ['uid', 'name']);
      $query->fields('ti', ['nid', 'type', 'title', 'status']);
      $query->condition('ti.tid', $term_data);
      // Add the condition on the content type, if content type is set.
      if (isset($content_type) && !empty($content_type)) {
        $query->condition('nfd.type', $content_type);
      }
      if (!empty($title)) {
        $query->condition('title', $title, '=');
      }
      if (!empty($user_name)) {
        $query->condition('u.name', $user_name, '=');
      }

      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
        ->orderByHeader($header);
      // Limit the rows to 3 for each page.
      $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(3);
      $result = $pager->execute()->fetchAll();

    }
    elseif (!empty($vocabulary && $term_data)) {
      $query = $this->database->select('taxonomy_index', 'ti');
      $query->join('node_field_data', 'nfd', 'nfd.nid = ti.nid');
      $query->join('users_field_data', 'u', 'nfd.uid=u.uid');
      $query->fields('nfd', ['nid', 'type', 'title', 'status']);
      $query->fields('u', ['uid', 'name']);
      $query->condition('ti.tid', $term_data);
      // Add the condition on the content type, if content type is set.
      if (isset($content_type) && !empty($content_type)) {
        $query->condition('nfd.type', $content_type);
      }
      if (!empty($title)) {
        $query->condition('title', $title, '=');
      }
      if (!empty($user_name)) {
        $query->condition('u.name', $user_name, '=');
      }
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
        ->orderByHeader($header);
      // Limit the rows to 3 for each page.
      $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(3);
      $result = $pager->execute()->fetchAll();
    }
    else {
      $query = $this->database->select('node_field_data', 'nfd');
      $query->join('users_field_data', 'u', 'nfd.uid=u.uid');
      $query->fields('nfd', ['nid', 'type', 'title', 'status']);
      $query->fields('u', ['uid', 'name']);
      if (isset($content_type) && !empty($content_type)) {
        $query->condition('nfd.type', $content_type);
      }
      if (!empty($title)) {
        $query->condition('title', $title, '=');
      }
      if (!empty($user_name)) {
        $query->condition('u.name', $user_name, '=');
      }
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
        ->orderByHeader($header);
      // Limit the rows to 3 for each page.
      $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(3);
      $result = $pager->execute()->fetchAll();
    }
    $data = [];
    foreach ($result as $value) {
      $edit_url = new FormattableMarkup('<a href=":link">:label</a>', [
        ':link' => Url::fromRoute('entity.node.edit_form', ['node' => $value->nid])->toString(),
        ':label' => 'Edit',
      ]);
      $view_url = new FormattableMarkup('<a href=":link">:label</a>', [
        ':link' => Url::fromRoute('entity.node.canonical', ['node' => $value->nid])->toString(),
        ':label' => 'View',
      ]);
      $operation = new FormattableMarkup('@edit_link | @view_link',
      ['@edit_link' => $edit_url, '@view_link' => $view_url]);
      $data[] = [
        'title' => Markup::create('<a href="' . $base_path . '/node/' . $value->nid . '">'
        . $value->title . '</a>'),
        'type' => $value->type,
        'author' => Markup::create('<a href="' . $base_path . '/node/' . $value->uid . '">' . $value->name . '</a>'),
        'status' => $value->status == 1 ? 'Published' : 'Unpublished',
        'edit' => $operation,
      ];

    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $data,
      '#empty' => $this->t('No Data found'),
    ];

    // Finally add the pager.
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

}
