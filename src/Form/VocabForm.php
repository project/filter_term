<?php

namespace Drupal\filter_term\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\taxonomy\VocabularyStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an term load basis on vocabulary form.
 */
class VocabForm extends FormBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs for a Default controller.
   *
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   The vocabulary storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Request $current_request, VocabularyStorageInterface $vocabulary_storage, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRequest = $current_request;
    $this->vocabularyStorage = $vocabulary_storage;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vocab_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vocabulary = NULL, $term = NULL, $keys = NULL) {

    $vocabulary = $this->currentRequest->query->get('vocabulary');
    $term_data = $this->currentRequest->query->get('term');
    $title = $this->currentRequest->query->get('title');
    $content_type = $this->currentRequest->query->get('content_type');
    $user_role = $this->currentRequest->query->get('users');

    $vocabularies = $this->vocabularyStorage->loadMultiple();

    $options = [];
    $options = ['-select-'];
    foreach ($vocabularies as $voc) {
      // Get label names of vocabulary.
      $options[$voc->id()] = $voc->label();
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $options,
      '#default_value' => !empty($vocabulary) ? $vocabulary : '',
      '#ajax' => [
        'callback' => '::reloadTermCallback',
        'wrapper' => 'term-field-wrapper',
        'event' => 'change',
      ],
    ];

    $vocab_list = $form_state->getValue('vocabulary');
    if (!empty($vocab_list)) {
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocab_list);
      $term_tree = [];
      $term_tree = ['-select-'];
      foreach ($terms as $term) {
        $term_tree[$term->tid] = $term->name;
      }
    }
    else {
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary);
      $term_tree = [];
      $term_tree = ['-select-'];
      foreach ($terms as $term) {
        $term_tree[$term->tid] = $term->name;
      }
    }

    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Taxonomy Term'),
      '#options'   => $term_tree,
      '#default_value' => !empty($term_data) ? $term_data : '',
      '#prefix' => '<div id="term-field-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#attributes' => ['placeholder' => 'Search Node Title'],
      '#default_value' => !empty($title) ? $title : '',
    ];

    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // If you need to display them in a drop down:
    $options = [];
    $options = ['-select-'];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $options,
      '#default_value' => !empty($content_type) ? $content_type : '',
      '#executes_submit_callback' => TRUE,
    ];

    // Users list in drop down.
    $userStorage = $this->entityTypeManager->getStorage('user');
    $query = $userStorage->getQuery();
    $uids = $query
      ->condition('status', '1')
      ->accessCheck(TRUE)
      ->execute();
    $users = $userStorage->loadMultiple($uids);
    $author_options = [];
    $author_options = ['-select-'];
    foreach ($users as $user) {
      // $users[1]->values["name"]["x-default"]
      $name = $user->get('name')->value;
      // $roles = $user->getRoles();
      $author_options[] = $name;
    }
    $form['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Author'),
      '#default_value' => !empty($user_role) ? $user_role : '',
      '#options' => $author_options,
      '#executes_submit_callback' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
    ];
    $form['#attached']['library'][] = 'filter_term/filter_term_styles';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reloadTermCallback(array &$form, FormStateInterface $form_state) {
    return $form['term'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('title');
    $content_type = $form_state->getValue('content_type');
    $users = $form_state->getValue('roles');
    $term_data = $form_state->getValue('term');
    $vocabulary = $form_state->getValue('vocabulary');
    $url = Url::fromRoute('filter_term.allcontent')
      ->setRouteParameters([
        'title' => $title,
        'content_type' => $content_type,
        'users' => $users,
        'term' => $term_data,
        'vocabulary' => $vocabulary,
      ]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * Resets the filter selections.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('filter_term.allcontent');
    $form_state->setRedirectUrl($url);
  }

}
