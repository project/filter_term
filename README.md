## INTRODUCTION

The Filter Term module is filters and fetch all our nodes on 
the basis of vocabulary,
taxonomy term and content type and also fetch node by title and users name.
Show list of published and unpublished node and filter node by
content types.

## REQUIREMENTS

This module requires node and taxonomy of Drupal core.

## INSTALLATION

The installation of this module is like other Drupal modules.


1. If your site is managed via Composer,
use Composer to download the filter term module running
composer require "drupal/filter_term". Otherwise copy/upload the filter term
module to the modules directory of your Drupal installation.


2. Enable the 'Filter Term' module and got to path.
(/admin/config/filter_term/vocab)

## CONFIGURATION
1. Configure the module at Administration > Configuration > System > Vocab Form
2. Select the "Vocabulary" from dropdown, taxonomy terms will automatically
load for specific of vocabulary
select terms and content type and filter the nodes.
3. Select the "Author" and content type and fetch node basis on this.

## MAINTAINERS

Current maintainers for Drupal 10:

- Arti (arti_parmar) - https://www.drupal.org/u/arti_parmar
